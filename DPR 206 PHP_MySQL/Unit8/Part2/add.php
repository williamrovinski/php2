<?php
// make connection to database
require "dbinfo.php";

//clean and sanitize the incoming data 
if($_POST['submit']=="Submit") {
    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL); 
    $first = filter_var($_POST['first'], FILTER_SANITIZE_STRING);
    $last = filter_var($_POST['last'], FILTER_SANITIZE_STRING);
    $phone = filter_var($_POST['phone'], FILTER_SANATIZE_STRING);

    //check to see if your required fields are setup 
    validateForm($email, $first, $last, $phone); 
}

function validateForm($email, $first, $last, $phone){
    //your code to check and see if things are empty, regular expressions, etc. 
    if($phone == NULL)
        $phoneError = "Please enter a phone number";

    if($phoneError != '') {
        include('addform.php');
        exit();
    } else {
        addData($email, $first, $last, $phone);
    }
}//end validateForm
function addData()
{
    //make connection to database
    require "dbinfo.php";

    //setup a safe query
    $query = "INSERT INTO contacts VALUES(NULL, '$email', '$first', '$last', '$phone')";

    //run the query
    if ($result = mysqli_query($connection, $query)){

        // show confirmation
        include "header.php";
        echo "Contact added: $email, $first, $last, $phone<br>";
    } else {
        echo "Unable to add record."; 
    }
}//end addData
?>