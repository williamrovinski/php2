<?php
// make connection to database
    require "dbinfo.php";
//clean and sanitize the incoming data 
if ($_POST['submit']=="Submit"){
    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
    $first = filter_var($_POST['first'], FILTER_SANITIZE_STRING);
    $last = filter_var($_POST['last'], FILTER_SANITIZE_STRING);
    $phone = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
    $Happen = filter_var($_POST['Happen'], FILTER_SANITIZE_STRING);
    $Time = filter_var($_POST['Time'], FILTER_SANITIZE_STRING);
    $Many = filter_var($_POST['Many'], FILTER_SANITIZE_STRING);
    $Description = filter_var($_POST['Description'], FILTER_SANITIZE_STRING);
    $Make = filter_var($_POST['Make'], FILTER_SANITIZE_STRING);
    $Comments = filter_var($_POST['Comments'], FILTER_SANITIZE_STRING);

    //check to see if your required fields are setup
    validateForm($email, $first, $last, $phone, $Happen, $Time, $Many, $Description, $Make, $Comments);
}

function validateform($email, $first, $last, $phone, $Happen, $Time, $Many, $Description, $Make, $Comments){
    //your code to check and see if things are empty, regular expressions, etc.
    if($phone == NULL)
        $phoneError = "Please enter a phone number"; 

    if($phoneError != '') {
        include('addform.php');
        exit();
    } else {
        addData($email, $first, $last, $phone, $Happen, $Time, $Many, $Description, $Make, $Comments);
    }
}//end validateForm
    
function addData($email, $first, $last, $phone, $Happen, $Time, $Many, $Description, $Make, $Comments)
{
    // make connection to database
    require "dbinfo.php";

    // setup a safe query 
    $query = "INSERT INTO contacts VALUES (NULL, '$email', '$first', '$last', '$phone', '$Happen', '$Time', '$Many', '$Description', '$Make', '$Comments')";

// run the query 
    if ($result = mysqli_query($connection, $query)) {

        // show confirmation
        include "header.php";
        echo "Contact added: $email, $first, $last, $phone, $Happen, $Time, $Many, $Description, $Make, $Comments<br>";
    } else {
        echo "Unable to add record.";
    }
}//end addData    
?>
