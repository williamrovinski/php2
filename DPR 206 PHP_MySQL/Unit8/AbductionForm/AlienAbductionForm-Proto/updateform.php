<?php
//updateform.php
//connect to the database
require "dbinfo.php";
$sel_record = $_POST['sel_record'];

//SQL statement to select record to edit
$query = "SELECT * FROM contacts WHERE id = $sel_record";

// execute SQL query and get result
if ($result = mysqli_query($connection, $query)) {
    //loop through record and get values
    while ($record = mysqli_fetch_array($result)) {
        $id = $record['id'];
        $email = $record['email'];
        $first = $record['first']; 
        $last = $record['last'];
		$phone = $record['phone'];
		$Happen = $record['Happen'];
		$Time = $record['Time'];
		$Many = $record['Many'];
		$Description = $record['Description'];
		$Make = $record['Make'];
		$Comments = $record['Comments'];
    }    // end while loop 
} else {
    print "<h1>Something has gone wrong!</h1>";
    exit();
} //end else

$pageTitle = "Edit a Contact";
include "header.php";
print <<<HERE
	<h2>Modify this Contact</h2>
    <p>Change the values in the text boxes then click the "Modify Record" button.</p>

	<form id = "myForm" method="POST" action = "update.php">
        <input type="hidden" name="id" value="$id">
	<div>
		<div class="container1"><b>*First Name:</b></div><input type="text" id="textBox" name="first" value="$first">
        	<br />
           
    	<div class="container1"><b>*Last Name:</b></div><input type="text" id="textBox" name="last" value="$last">
			<br />
		
		<div class="container1"><b>Phone:</b></div><input type="text" id="textBox" name="phone" value="$phone">
        	<br />

        <div class="container1"><b>*Email:</b></div><input type="text" id="textBox" name="email" value="$email">
            <br />
           
        <div class="container1"><b>When did it happen?</b></div><input type="text" id="textBox" name="Happen" value="$Happen">
           <br />
           
        <div class="container1"><b>How Long were you gone?</b></div><input type="text" id="textBox" name="Time" value="$Time">
           <br />
           
        <div class="container1"><b>How many did you see?</b></div><input type="text" id="textBox" name="Many" value="$Many">
           <br />
           
        <div class="container1"><b>Describe them:</b></div><input type="text" id="textBox" name="Description" value="$Description">
           <br />
           
        <div class="container1"><b>What did they do to you?</b></div><input type="text" id="textBox" name="Make" value="$Make">
		   <br />
		   
		   <label id="Fluffy"><b>*Have you seen Fluffy?</b></label>
           <span id="space"></span>
				   
		  	 <b>Yes</b><input type="radio" name="RadioButton" value="Yes" id="Yes"         
			 <b>No</b><input type="radio" name="RadioButton" value="No" id="No"><br>
			  
			<br>
			<div class="container1"><b>Anything else you want to add?</b></div><textarea name="Comments" rows="3" cols="20" id="textBox" value="$Comments"></textarea>
	    
	<div id="mySubmit">
	    <input type="submit" name="submit" value="Modify Record">
	</div>
	</form>

HERE;
 
?>