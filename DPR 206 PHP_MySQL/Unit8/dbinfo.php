<?php
// database connection information
$host = "localhost";
$user = "yourdatabaseUsername";
$password = "yourdatabasePassword";
$database = "yourdatabaseName";

$connection = @mysqli_connect($host, $user, $password, $database); 
if(mysqli_connect_error()){
    die('Connect Error: ' . mysqli_connect_error());
} else {
    echo 'Successful connection to database';
}

?>