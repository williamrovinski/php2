<!DOCTYPE html>
   <html lang="en">
    <head>
    <meta charset="utf-8">
    <title>Alien Form Feedback</title>

    <!-- Script for alien abduction form-->
    <?php 
    
    // Create my variables to be stored. 
    $FirstName = $_REQUEST['FirstName'];
    $LastName = $_REQUEST['LastName']; 
    $Email = $_REQUEST['Email'];
    $happen = $_REQUEST['happen'];
    
    $Long = $_REQUEST['Long'];
    $Many = $_REQUEST['Many'];
    $Describe = $_REQUEST['Describe'];
    $Make = $_REQUEST['Make'];
    $message = $_REQUEST['message'];

    // Print submitted information
    echo "<p>Thank you for submitting the form <strong>$FirstName</strong> <strong>$LastName</strong>.</p>";
    echo "<p>You were abducted on <strong>$happen</strong>, and gone for <strong>$Long</strong>.</p>"; 
    echo "<p>You said there were(was) <strong>$Many</strong> of them.</p>";
    echo "<p>They <strong>$Make</strong>.</p>";
    echo "<p>You described them as <strong>$Describe</strong>.</p>";
    
    // Print radio button message
    if(isset($_POST['RadioButton'])) {
        $RadioButton = $_POST['RadioButton'];
        echo "You selected " . $RadioButton;
    }
    else {
        echo "You forgot to answer whether you saw the dog or not."; 
    }
  

    echo "<p>Your other comments were: <strong>$message</strong>.</p>";  
    echo "<p>We will contact you at <strong>$Email</strong> if we have relevant news.</p>"; 

    $to = "13rovinski@cua.edu";
    $subject = "Aliens Abduction Form"; 
    $body ="$FirstName $LastName - $Email \n Date: $date \n";
    mail($to, $subject, $body); 

    //Vallidate the name information: 
        if (!empty($_REQUEST['FirstName'])) {
            $FirstName = $_REQUEST['FirstName'];
        } elseif (!empty($_REQUEST['LastName'])) {
            $LastName = $_REQUEST['LastName'];      
        } else {
            $name = NULL;
            echo "<p>Please enter a first and last name.</p>";    
        }

    //Vallidate happen and Long.
        if (!empty($_REQUEST['happen'])) {
            $happen = $_REQUEST['happen'];
        } elseif(!empty($_REQUEST['Long'])) {
            $Long = $_REQUEST['Long'];
        } else {
            $happen = NULL;
            $Long = NULL;
            echo "<p>Please enter what happened.</p>"; 
        }
    //Vallidate Many.
        if(!empty($_REQUEST{'Many'})) {
            $Many = $_REQUEST['Many'];
        } else {
            $Many = NULL; 
            echo "<p>Please enter how many there were/was.</p>"; 
        }
    //Vallidate Make.
        if(!empty($_REQUEST{'Make'})) {
            $Make = $_REQUEST['Make'];
        } else {
            $Make = NULL; 
            echo "<p>Please tell what they made you do.</p>"; 
        }
    //Vallidate Describe.
    if(!empty($_REQUEST{'Describe'})) {
        $Describe = $_REQUEST['Describe'];
    } else {
        $Describe = NULL; 
        echo "<p>Please described them.</p>"; 
    }
    //Vallidate message.
    if(!empty($_REQUEST{'message'})) {
        $message = $_REQUEST['message'];
    } else {
        $message = NULL; 
        echo "<p>Please enter some comments of you wish.</p>"; 
    }
    //Vallidate Email.
    if(!empty($_REQUEST{'Email'})) {
        $Email = $_REQUEST['Email'];
    } else {
        $Email = NULL; 
        echo "<p>Please enter your email.</p>"; 
    }
    ?>

    </body>
    </html>