<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Testing PCRE</title>
</head>
<?php
// This Script takes a submitted string and checks it against a submitted pattern.
        $matches = '';
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // Trim the strings:
        $pattern = trim($_POST['pattern']);
        $subject = trim($_POST['subject']);
        $replace = trim($_POST['replace']);
    

        // Print a caption:
        echo "<p>The result of checking<br><strong>$pattern</strong><br>against<br>$subject<br>is
    ";


        // Check for a match:
        if (preg_match($pattern, $subject) ) {
            echo preg_replace($pattern, $replace, $subject) . '</p>';
        } else {
            echo 'The pattern was not found!</p>';
        }

        // Test:
        if (preg_match_all($pattern, 
        $subject, $matches) ) {
            echo 'TRUE!</p>';
           
            // Print the matches:
            echo '<pre>' . print_r($matches, 1) . '</pre>';
        
        } else {
            echo 'FALSE!</p>';
        }
    }
//End of submission IF.
// Copy and paste your work, or start typing.
$temp = "";
if(is_array($matches)) {
  echo $matches;
   foreach($matches as $m){
      $temp .= $m . "; ";
     
   }
  $matches = $temp;
  
}
?>

        <form action="pcre.php" method="post">
            <p>Regular Expression Pattern: <input type="text" name="pattern" value="<?php if (isset($pattern))
            echo htmlentities($pattern); ?>" size="40"> (include the delimeters)</p>
                <p>Replacement: <input type="text" name="replace" value="<?php if (isset($replace))
                echo htmlentities($replace) ; ?>" size="40"></p>
                <p>Test Subject: <textarea name="subject" rows="5" cols="40"><?php if (isset($subject))
                echo htmlentities($matches); ?></textarea></p>
                <input type="submit" name="submit" value="Test!">
        </form>
<?php
    /*if (preg_match ('/^(\d{5})(-\d{4})?$/')) {
    preg_match($pattern, $subject, $match);
        preg_match_all($pattern,$subject, 
            $matches);
            if (prep_match_all($pattern,
                $subject, $matches) ) {}
        }*/
?>
        </body>
        </html>
