<?php
//$age = $_POST['age'];
//$email = $_POST['email_address'];
//$major = $_POST['major'];
$errorAge = '';
$errorEmail = '';
$errorMajor = '';
$age = filter_input(INPUT_POST, 'age', FILTER_VALIDATE_INT);
$email = filter_input(INPUT_POST, 'email_address', FILTER_VALIDATE_EMAIL);
$major = filter_input(INPUT_POST, 'major');

if(!$age) {
    $errorAge = "You must enter a number.";
}
if(!$email) {
    $errorEmail = "You must enter an email.";
}
if(!$major) {
    $errorMajor = "You must enter a major.";
}    

?>
<!DOCTYPE html>
<html>
<head>
    <title>Demonstration of Using filter_input</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<main>
    <h1>Demonstration of Using filter_input</h1>
    <p>Email:<?php echo " " . $email , $errorEmail?></p>
    <p>Age: <?php echo $age , $errorAge ?></p> <!-- you can also concatenate with a comma, however 
                                                        it is really interchangable with a period.--> 
    <p>Major: <?php echo " " . $major , $errorMajor ?></p>
</main>
</body>
</html>

