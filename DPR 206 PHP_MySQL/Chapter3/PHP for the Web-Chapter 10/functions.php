<!doctype html>
   <html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Functions</title>
  </head>
  <body>
  <?php 
    function add_one($n) {
        $n++;
        print 'Added one!<br>';
    }
    $n = 1; # n is defined here as being 1 and thus even though we call add_one function later, it matters not 
            # as this n variable is different than the one we defined in the function. In fact it is treated as 
            # a parameter then later n newly defined is called back as an argument  
    print "\$n equals $n<br>"; 
    add_one($n);
    print "\$n equals $n<br>";

    echo "<br>";

    function add_two() {
        global $n; // Same! When we state global, the variable will be treated as the value set by global 
        $n++;
        print 'Added one!<br>';
    }
    $n = 1;
    print "\$n equals $n<br>";
    add_two();
    print "\$n equals $n<br>"; 
  
  ?>
</body>
</html>