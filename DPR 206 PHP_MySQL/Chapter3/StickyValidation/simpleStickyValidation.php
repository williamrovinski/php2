<!DOCTYPE html>
<html lang="en">
<head>
<title>Sticky Validation</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <h2>A Simple Form for Sticky Validation</h2>
    <?php
    $nameError = "";
    $emailError = "";
        
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
               validateStuff();
        } else {
            showForm($nameError, $emailError, $name, $email);
        }

        function validateStuff() {
            $name = $_POST['name'];
            $email = $_POST['email'];
    
            if(empty($name)) {
                $name = NULL;
                $nameError = "<p>Please enter your first name.</p>";
            }
            if(empty($email)) {
                $email = NULL;
                $emailError = "<p>Please enter your email address.</p>";
            }

            if (!($name && $email))
            {
                showForm($nameError, $emailError, $name, $email);
            }
            if($name && $email) {
            //display an output message to user
            print "Thanks for signing up for our mailing list <b> $name.</b>";
            print "<p>We will send our newsletter to you at <b> $email</b>.</p>";
            }
        } // end validateStuff
    function showForm($nameError, $emailError, $name, $email) {
        print <<< FORM
        <form method="post" action="">
        <div class>
        <label for="name">*Name:</label>
        <input name="name" type="text" id="name" placeholder="name" value= "$name">
        $nameError
        </div>

        <div class="clear">
        <label for="email">*What is your email address?</label>
        <input name="email" type="email" id="email" placeholder="email" value= "$email">
        $emailError
        </div>

        <div class="clear">
        <input type="submit" value="Sign Up" name="submit" class="Submit">
        </div>
        
</form> 
FORM;

    }
    ?>


</body>
</html>