<!DOCTYPE html>
<html lang="en">
<head>
    <title>PHP functions</title>
    <meta charset="utf-8">
    </head>
    <body>
    <?php
    $firstName = "Professor";
    $lastName = "Dom Mazzetti";

    function bigDom($firstName, $lastName) {
        echo "My name is $firstName $lastName.<br>";
        echo "Happy Swolloween."; 
    }
    bigDom($firstName, $lastName); 
    echo "<br>";

    function myName($first, $last) {
        echo "<h1> Hello, World!</h1>\n";
        echo "My Name is $first $last.";
    }
    myName("Professor", "Gustin"); 
     

    ?>
    </body>
</html>