<?php
	//get the data from the form, filter it
		$sampleBox = $_POST[sampleBox];
    	$sampleList = $_POST[sampleList];
    	$radioExample = $_POST[radioExample];
    	$myComments = $_POST[myComments];

	
	//if sampleBox is empty, display error message
	if ($sampleBox == NULL) {
		$sampleBoxError = 'Please enter some text';
	}
	
	//if sampleList is empty, display error message
	if ($sampleList == NULL) {
		$sampleListError = 'Please select a list item';
	}

	//if radioExample is empty, display error message
	if ($radioExample == NULL) {
		$radioExampleError = 'Please select a radio button.';
	}

	//if myComments is empty, display error message
	if ($myComments == NULL) {
		$myCommentsError = 'Please give us your comments.';
	}
	// if an error message exists, show the form page
	if(($sampleBoxError != '') || ($sampleListError != '')
	|| ($radioExampleError != '') || ($myCommentsError != '')) {
		include('myform.php');
		exit();
	}else{
		print <<<MESSAGE

		<p>You entered $sampleBox.</p>
		<p>You selected $sampleList from the list.</p>
		<p>You said $radioExample.</p>
		<p>Your comments were $myComments.</p>

MESSAGE;

	}
?>
