<?php
//set default value of variables for initial page load
if (!isset($sampleBoxError)) {$sampleBoxError = ''; }
if (!isset($sampleListError)) { $sampleListError = ''; }
if (!isset($radioExampleError)) { $radioExampleError = ''; }
if (!isset($myCommentsError)) { $myCommentsError = ''; }

if (!isset($sampleBox)) { $sampleBox = ''; }
if (!isset($sampleList)) { $sampleList = ''; }
if (!isset($radioExample)) { $radioExample = ''; }
if (!isset($myComments)) { $myComments = ''; }
?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Test Form</title>
<meta charset="utf-8">
    <style type="text/css">
    form { background-color: #eaeaea;
              font-family: Arial, sans-serif;
              width: 800px;
              padding: 10px;
              }
    label {   display: block;
              float: left;
              width: 200px;
              clear: left;
              text-align: right;
              padding-right:  20px;
              margin-top: 15px;
              }         
    input, textarea, select {  margin-top: 15px;}
    #mySubmit { margin-left:  200px;
                padding-bottom: 10px;
                }
    </style>
</head>
<body>
<h2>Test Form</h2>
<form  action="results.php" method="post" id="myForm">
  <div>
      <label for="sampleBox">A textbox</label>
      <input name="sampleBox" type="text" id="sampleBox"
      value ="<?php print $sampleBox; ?>">
      <?php print $sampleBoxError; ?>
  </div>
  
  <div>
      <label for="sampleList">A select list</label>
        <select name="sampleList" id="sampleList">
          <option value="" <?php if($sampleList == "") echo "selected" ?>>Select One</option>
          <option value="itemA" <?php if($sampleList == "itemA") echo "selected"?>>Item A</option>
          <option value="itemB" <?php if($sampleList == "itemB") echo "selected"?>>Item B</option>
          <option value="itemC" <?php if($sampleList == "itemC") echo "selected"?>>Item C</option>
          <option value="itemD" <?php if($sampleList == "itemD") echo "selected"?>>Item D</option>
        </select>
        <?php print $sampleListError ?> 
  </div>
  
  <div>
       <label>A radio button group</label>
        <input type="radio" name="radioExample" value="yes" id="radioExampleYes"
        <?php if($radioExample == "yes") echo "checked" ?>>
            Yes <br>
        <input type="radio" name="radioExample" value="no" id="radioExampleNo">
            No <br>
            <?php print $radioExampleError; ?>   
  </div>
  
  <div>
       <label for="myComments">Comments</label>      
        <textarea name="myComments" id="myComments" rows="3" cols="20">
        <?php print $myComments; ?>
        </textarea>
        <?php print $myCommentsError; ?>
  </div>
  
  <div id="mySubmit">
    <input type="submit" name="Submit" value="Submit">
    <input type="reset" name="Reset" value="Reset">
 </div> 
</form>

</body>
</html>