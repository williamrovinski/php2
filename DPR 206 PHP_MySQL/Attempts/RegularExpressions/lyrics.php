<html>
<head>
<title>Regular Expressions</title>
</head>
<body>
<?php
    $pattern = '/something to look for/';
    $lyrics = "Well, my daddy left home when I was three,
and he didn't leave much to Ma and me,
just this old guitar and a bottle of booze.
Now I don't blame him because he run and hid,
but the meanest thing that he ever did was
before he left he went and named me Sue.

Well, he must have thought it was quite a joke,
and it got lots of laughs from a lot of folks,
it seems I had to fight my whole life through.
Some gal would giggle and I'd get red
and some guy would laugh and I'd bust his head,
I tell you, life ain't easy for a boy named Sue.

Source: https://www.familyfriendpoems.com/poem/a-boy-named-sue-by-shel-silverstein";
      

?>
</body>
</html>