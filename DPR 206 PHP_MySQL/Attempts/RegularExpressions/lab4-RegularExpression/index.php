<?php
  //set default values of variables for initial page load
  if (!isset($FirstNameError)) {$FirstNameError = ''; }
  if (!isset($LastNameError)) {$LastNameError = ''; }
  if (!isset($EmailError)) {$EmailError = ''; }
  if (!isset($RadioButtonError)) {$RadioButttonError = ''; }
  if (!isset($HappenError)) {$HappenError = ''; }
  if (!isset($LongError)) {$LongError = ''; }
  if (!isset($ManyError)) {$ManyError = ''; }
  if (!isset($DescribeError)) {$DescribeError = ''; }
  if (!isset($MakeError)) {$MakeError = ''; }
  if (!isset($messageError)) {$messageError = ''; }

  if (!isset($FirstName)) {$FirstName = ''; }
  if (!isset($LastName)) {$LastName = ''; }
  if (!isset($Email)) {$Email = ''; }
  if (!isset($RadioButton)) {$RadioButtton = ''; }
  if (!isset($Happen)) {$Happen = ''; }
  if (!isset($Long)) {$Long = ''; }
  if (!isset($Many)) {$Many = ''; }
  if (!isset($Describe)) {$Describe = ''; }
  if (!isset($Make)) {$Make = ''; }
  if (!isset($message)) {$message = ''; }
?>

<!DOCTYPE html>
 <html lang="en-US">
 <html>
   <head>
    
    <title>Alien Abduction Fanfiction</title>
    <link rel="stylesheet" type="text/css" href="AlienStory.css">

   </head>
       <body>
        <form action="report.php" method="post">

           <h2>Aliens Abducted Me - Report an Abduction</h2>

           <p>Share your story of alien abduction:</p>
           <p>Required Fields*</p>
           <br>

           <div class="container1"><b>*First Name:</b></div><input type="text" id="textBox" name="FirstName" value="<?php echo $FirstName; ?>">
           <?php echo $FirstNameError; ?><br />
           
           <div class="container1"><b>*Last Name:</b></div><input type="text" id="textBox" name="LastName" value="<?php print $LastName; ?>">
           <?php print $LastNameError; ?><br />

           <div class="container1"><b>*Email:</b></div><input type="text" id="textBox" name="Email" value="<?php print $Email; ?>">
           <?php print $EmailError; ?> <br />
           
           <div class="container1"><b>When did it happen?</b></div><input type="text" id="textBox" name="Happen" value="<?php echo $Happen; ?>">
           <?php print $HappenError?><br />
           
           <div class="container1"><b>How Long were you gone?</b></div><input type="text" id="textBox" name="Long" value="<?php print $Long; ?>">
           <?php echo $LongError; ?><br />
           
           <div class="container1"><b>How many did you see?</b></div><input type="text" id="textBox" name="Many" value="<?php print $Many; ?>">
           <?php echo $ManyError ?><br />
           
           <div class="container1"><b>Describe them:</b></div><input type="text" id="textBox" name="Describe" value="<?php echo $Describe; ?>">
           <?php print $DescribeError; ?><br />
           
           <div class="container1"><b>What did they do to you?</b></div><input type="text" id="textBox" name="Make" value="<?php echo $Make; ?>">
           <?php print $MakeError; ?><br />
          
           
           <label id="Fluffy"><b>*Have you seen Fluffy?</b></label>
           <span id="space"></span>
           <b>Yes</b><input type="radio" name="RadioButton" value="Yes" id="Yes" 
           <?php if (isset($_POST['RadioButton'])) { if($_POST['RadioButton'] =='Yes' || 'No') 
             {echo 'checked = "checked"';}}
             
             {if 
               (!(isset($_POST['RadioButton']))) {echo 'required field';}} 
             
           ?>>
            
           <b>No</b><input type="radio" name="RadioButton" value="No" id="No"><br>
          
           <img src="Fluffy.jpeg" alt="Fluffy"><br>
           <div class="container1"><b>Anything else you want to add?</b></div><textarea name="message" rows="3" cols="20" id="textBox"></textarea>
           <br><button type="submit" id="buttonPress" name="Report Abduction">Report Abduction</button>
           
        </form>
       </body>
  </html>