    <?php 

    //Get the data from the form
    $FirstName = $_POST['FirstName'];
    $LastName = $_POST['LastName'];
    $Email = filter_input(INPUT_POST, 'Email', FILTER_VALIDATE_EMAIL);
    $RadioButton = $_POST['RadioButton'];
    $Happen = $_POST['Happen'];
    $Long = $_POST['Long'];
    $Many = $_POST['Many'];
    $Describe = $_POST['Describe'];
    $Make = $_POST['Make'];
    $message = $_POST['message'];

    // Cleearing error messages
    $FirstNameError = '';
    $LastNameError = '';
    $EmailError = '';
    $RadioButtonError = '';
    $HappenError = '';
    $LongError = '';
    $ManyError = '';
    $DescribeError = '';
    $MakeError = '';
    $messageError = '';


    //If FirstName is empty, error message
    if ($FirstName == NULL) {
        $FirstNameError = 'Please enter your first name';
    }

    //If LastName is empty, error message
    if ($LastName == NULL) {
        $LastNameError = 'Please enter your last name.';
    }

    //If Email is empty, error message
    if ($Email == NULL) {
        $EmailError = 'Please enter your email.';
    }

    //If RadioButton is empty, error message
    if ($RadioButton == NULL) {
        $RadioButtonError = 'Please tell if you saw Fluffy or not.'; 
    }

    //If Happen is empty, error message
    if ($Happen == NULL) {
        $HappenError = 'Please enter when it happened'; 
    }
    //If Long is empty, error message
    if ($Long == NULL) {
        $LongError = 'Please tell how long were you gone'; 
    }

    //If Many is empty, error message
    if ($Many == NULL) {
        $ManyError = 'Please tell how many there were/was.'; 
    }

    //If Describe is empty, error message
    if ($Describe == NULL) {
        $DescribeError = 'Please describe what they looked like.'; 
    }

    //If Make is empty, error message
    if ($Make == NULL) {
        $MakeError = 'Please tell what they made you do.'; 
    }

    //Email the form 
    $to = "13rovinski@cua.edu";
    $subject = "Aliens Abduction Form"; 
    $body ="$FirstName $LastName - $Email \n Date: $date \n";
    mail($to, $subject, $body); 

    // if an error messagae exists, show the form page
    if (($FirstNameError != '') || ($LastNameError != '') || ($EmailError != '') || ($RadioButtonError != '') || ($HappenError != '') || 
    ($LongError != '') || ($ManyError != '') || ($DescribeError != '') || ($MakeError != '') || ($messageError != '')) {
		include('index.php');
		exit();
	}else{
        print <<<MESSAGE

        <p>Thank you for submitting the form <strong>$FirstName</strong> <strong>$LastName</strong>.</p>
        <p>You were abducted on <strong>$Happen</strong> ,and gone for <strong>$Long</strong>.</p>
        <p>You said there were(was) <strong>$Many</strong> of them.</p>
        <p>They <strong>$Make</strong>.</p>
        <p>You described them as <strong>$Describe</strong>.</p>
        <p>$RadioButton ' you did/didn't see Fluffy'</p>
        <p>Your other comments were: <strong>$message</strong>.</p>
        <p>We will contact you at <strong>$Email</strong> if we have relevant news.</p>
MESSAGE;
    }
    ?>