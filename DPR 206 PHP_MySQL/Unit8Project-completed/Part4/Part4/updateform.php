<?php
//updateform.php
//connect to the database
require "dbinfo.php";
$sel_record = $_POST['sel_record'];

//SQL statement to select record to edit
$query = "SELECT * FROM contacts WHERE id = $sel_record";

// execute SQL query and get result
if ($result = mysqli_query($connection, $query)) {
    //loop through record and get values
    while ($record = mysqli_fetch_array($result)) {
        $id = $record['id'];
        $email = $record['email'];
        $first = $record['first']; 
        $last = $record['last'];
        $phone = $record['phone'];
    }    // end while loop 
} else {
    print "<h1>Something has gone wrong!</h1>";
    exit();
} //end else

$pageTitle = "Edit a Contact";
include "header.php";
print <<<HERE
	<h2>Modify this Contact</h2>
    <p>Change the values in the text boxes then click the "Modify Record" button.</p>

	<form id = "myForm" method="POST" action = "update.php">
        <input type="hidden" name="id" value="$id">
	<div>
	    <label for="email">Email*:</label>
	    <input type="text" name="email" id="email" value="$email">
	</div>
	<div>
	    <label for="first">First Name*:</label>
	    <input type="text" name="first" id="first" value="$first">
	</div>
	<div>
	    <label for="last">Last Name*:</label>
	    <input type="text" name="last" id="last" value="$last">
	</div>
	<div>
	    <label for="phone">Phone*:</label>
	    <input type="text" name="phone" id="phone" value="$phone">
	</div>
	<div>
	    <label for="image">Image:</label>
	    <input type="text" name="image" size="30" id="upload">
	</div>
	<div id="mySubmit">
	    <input type="submit" name="submit" value="Modify Record">
	</div>
	</form>

HERE;
 
?>