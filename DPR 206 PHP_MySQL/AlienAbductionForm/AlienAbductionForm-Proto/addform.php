<?php
//set default value of variables for initial page load
if(!isset($phoneError)) { $phoneError = ''; }
if(!isset($phone)) { $phone = ''; }

include("header.php");
print <<<HERE
	<h2>Add a Contact</h2>
	<form action="addsimple.php" method="POST" enctype="multipart/form-data">
	<div>
	    <label for="email">Email*:</label>
	    <input type="text" name="email" id="email" required="required">
	</div>

	<div>
	    <label for="first">First Name*:</label>
	    <input type="text" name="first" id="first" required="required">
	</div>

	<div>
	    <label for="last">Last Name*:</label>
	    <input type="text" name="last" id="last" required="required">
	</div>

	<div>
	    <label for="phone">Phone*:</label>
	    <input type="text" name="phone" id="phone" value="$phone"> $phoneError
	</div>

	<div>
	    <label for="WhenItHappened">When did it Happen:</label>
	    <input type="text" name="Happen" id="Happen" required="required">
	</div>

	<div>
	    <label for="HowLongWereYouGone">How Long Were You Gone:</label>
	    <input type="text" name="Time" id="Time" required="required">
	</div>

	<div>
	    <label for="HowManyDidYouSee">How Many Did You See:</label>
	    <input type="text" name="Many" id="Many" required="required">
	</div>

	<div>
	    <label for="DescribeThem">Describe Them:</label>
	    <input type="text" name="Description" id="Description" required="required">
	</div>

	<div>
	    <label for="Make">What Did They Make You Do:</label>
	    <input type="text" name="Make" id="Make" required="required">
	</div>

	<div>
	    <label for="Comments">Anything Else You Want To Add:</label>
	    <input type="text" name="Comments" id="Comments" required="required">
	</div>

	<div>

	<div id="mySubmit">
	    <input type="submit" name="submit" value="Submit">
	</div>
	</form>
HERE;

?>