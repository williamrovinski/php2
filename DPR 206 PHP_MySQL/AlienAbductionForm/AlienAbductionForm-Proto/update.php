<?php
//update.php
//connect to the database
require ("dbinfo.php");

//clean and sanitize the incoming data
if(isset($_POST['submit'])=="submit" && $_POST['submit']=="Modify Record") {
    $id = filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT);
    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
    $first = filter_var($_POST['first'], FILTER_SANITIZE_STRING);
    $last = filter_var($_POST['last'], FILTER_SANITIZE_STRING);
    $phone = filter_var($_POST['phone'], FILTER_SANITIZE_STRING);
    $Happen = filter_var($_POST['Happen'], FILTER_SANITIZE_STRING);
    $Time = filter_var($_POST['Time'], FILTER_SANITIZE_STRING);
    $Many = filter_var($_POST['Many'], FILTER_SANITIZE_STRING);
    $Description = filter_var($_POST['Description'], FILTER_SANITIZE_STRING);
    $Make = filter_var($_POST['Make'], FILTER_SANITIZE_STRING);
    $Comments = filter_var($_POST['Comments'], FILTER_SANITIZE_STRING);
}

//create a safe sql query
$query= "UPDATE contacts SET
    email='$email',
    first='$first',
    last='$last',
    phone='$phone',
    Happen='$Happen',
    Time='$Time',
    Many='$Many',
    Description='$Description',
    Make='$Make',
    Comments='$Comments'
    where id='$id' "; 


if ($result = mysqli_query($connection, $query)) {
    //show confirmation
    print "<html><head><title>Update Results</title></head><body>";
    $pageTitle = "Record Updated";
    include ("header.php");
    print <<<HERE
    <h1>The new record looks like this: </h1>
    <p><strong>E-mail:</strong> $email</p>
    <p><strong>First:</strong> $first</p>
    <p><strong>Last:</strong> $last</p>
    <p><strong>Phone:</strong> $phone</p>
    <p><strong>Happen:</strong> $Happen</p>
    <p><strong>Time:</strong> $Time</p>
    <p><strong>Many:</strong> $Many</p>
    <p><strong>Describe:</strong> $Description</p>
    <p><strong>Make:</strong> $Make</p>
    <p><strong>Comments:</strong> $Comments</p>
HERE;
}else{
    print "<h1>Something has gone wrong!</h1>";
    exit();
}//end else

?>