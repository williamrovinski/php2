<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Preventing XSS Attacks</title>
    <link rel="stylesheet" type="text/css" href="main.css">
    
</head>
<body>
<main>
    <h1>Preventing XSS Attacks</h1>
<form action="display.php" method="post">

    <label>Comments: </label>
    <textarea name="comments" rows="6" cols="50">
        Take that!
            <script type="text/javascript">
                alert('Got ya!');
            </script>
    </textarea>

    <label>&nbsp;</label>
    <input type="submit" value="Submit"/>
</form>
</main>
</body>
</html>

<!--
Take that!
<script type="text/javascript">
alert('Got ya!');
</script>
-->
