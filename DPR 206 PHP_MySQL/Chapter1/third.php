<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Predefined Variables</title>
    </head>
    <body>
        <!-- Script 1.2 - first.php-->
        <p>This is a standard HTML.</p>
        <?php 
        //Create the variables: 
        $first_name = 'Haruki';
        $last_name = 'Murakami';
        $book = 'Kafka on the Shore';


        //Print the values:
        echo "<p>The book <em>$book</em>
        was written by $first_name
        $last_name.</p>";

        ?>
    </body>
</html>